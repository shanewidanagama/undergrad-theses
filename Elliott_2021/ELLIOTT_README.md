1. Combo of RNA_EDA
Read dataset and set variables as numeric/categorical/factor. Include log(Time) transformation in the data frame

2. Correlations
Build a correlation matrix and visualize the correlations using the corrplot function. Determined which RNA metrics were not correlated to each other and could subsequently be used for regressions. 

3. Start of everything else, with combo of RNA, meaning data from both experiments
Simply an indicator than most of the analyses and plots are found below

4.EDA regression statistics
General statistics for each RNA metric, including summary statistics.

5. KS test to see if different before and after 24 hours - ended up using Unpaired Wilcoxon instead 
Used the Kolmogorov-Smirnov test to compare data before and after 24 hours. Ended up using unpaired Wilcoxon non-parametric tests instead but kept code if anyone is interested.

6. Wilcoxon test 
Determine if groups are significantly different from each other - non-parametric since data is not normally distributed, per Shapiro-Wilk results. Compared RNA metrics between donors and before and after 24 hours.

7. Linear regressions for all RNA metrics vs log (Time)
Build linear regressions, with and without donor (biological replicate) as a random effect. R^2 and slopes were recorded for each metric. 

8. Plots for practically everything
Plots range from 8.1 – 8.9, each representing a different RNA metric. Plot 8.1 is concentration, 8.2 is RINe, both of these were the most relevant to my study.

9. PCA - Principal component analysis of the data
Built a principal component analysis to investigate whether a combination of our linear variables had a better relationship with time than RINe, which was not the case. 

9.1 Contribution of variables - very cool and sharp tool
Very small part but very cool. A plot that indicates which of your original variables contributes to each principal component.
