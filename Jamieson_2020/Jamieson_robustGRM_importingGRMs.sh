#In R - standard import of GRM generated in GCTA and regeneration of GCTA compatible files
library(OmicKriging)
#Use functions readGRM and writeGRM below
grmFile="male_female_anticosti_lm90.grm.bin" 
grmFileBase <- substr(grmFile,1, nchar(grmFile) - 4)
GRM <- read_GRMBin(grmFileBase)
#make and id array
GRMid<-readGRM("male_female_anticosti_lm90") #this is the GRM prefix
dat2=GRMid$id 
#This produces a symmetrical matrix - which when re-written isn't the correct format for GCTA
#Need to vectorize and take lower diaganol of GRM
#Working format of R to produce usable GRM files is a two lists: one with ids, no. snps and R, the other ids and family. No. snps does not change relatedness values
GRM1=as.matrix(GRM)
nNrInd=437
#Note GRM in for loop needs to change to match above (i.e. GRM1)
GRM1r <- vector(mode = "numeric", length = sum(1:nNrInd))
nVecIdx <- 1
for (nRowIdx in 1:nrow(GRM1)){
  for (nColIdx in 1:nRowIdx){
    GRM1r[nVecIdx] <- GRM1[nRowIdx, nColIdx]
    nVecIdx <- nVecIdx + 1
  }
}
WriteGRMBin("GRM",grm=GRM1r,id=dat2,N=10000)
y=readGRM("GRM")
writeGRM(y,"GRM1r")

#Robust GRM generation
#In shell AND we borrow IDs from the equivalent GRM generated in GCTAl, in this case GRM1
FILE=XXX.vcf
#Need to impute missing data
#wget http://faculty.washington.edu/browning/beagle/beagle.25Nov19.28d.jar
#################
#Possible issues for BEAGLE to address
#sort VCFfile
#vcf-sort unsorted.vcf > sorted.vcf
#remove duplicate sites if needed in VCF
#vcflib/bin/vcfuniq VCF > newVCF
#remove single snp scaffolds
#grep -v "^#" VCFFILE | cut -f 1 | sort | uniq -c | sort -g > snp_scaf_count
#vcftools --not-chr CONTIG --recode --out
#################
#the Xmx8g is askign for 8g of memory - can tweak if needed
java -Xmx8g -Djava.awt.headless=true -jar beagle.25Nov19.28d.jar gt=$FILE impute=TRUE out=$FILE.impute.vcf
#need to generate geno file (0,1,2 for alterante (minor) alleles and frequency of alternatn (minor) allele
vcftools --vcf $FILE.impute.vcf --freq --max-alleles 2
cut -f6 out.frq | cut -f2 -d ':' | sed 1d > freq
vcftools --vcf $FILE --012
cut -f2- out.012 > geno
#In R
library(rres)
freq <- scan("freq")
out <- read.table("geno")
geno=as.matrix(out)
GRM2=grm.matrix(geno,freq,"robust")
rownames(GRM2)=rownames(GRM1)
colnames(GRM2)=colnames(GRM1)
#Same now as above
nNrInd=437
#Note GRM in for loop needs to change to match above (i.e. GRM2)
GRM2r <- vector(mode = "numeric", length = sum(1:nNrInd))
nVecIdx <- 1
for (nRowIdx in 1:nrow(GRM2)){
  for (nColIdx in 1:nRowIdx){
    GRM2r[nVecIdx] <- GRM2[nRowIdx, nColIdx]
    nVecIdx <- nVecIdx + 1
  }
}
WriteGRMBin("GRM2",grm=GRM2r,id=dat2,N=10000)
y=readGRM("GRM2")
writeGRM(y,"GRM2r")


######FUNCTIONS#########

writeGRM <- function(grm, rootname)
{
    bin.file.name <- paste(rootname, ".grm.bin", sep="")
    n.file.name <- paste(rootname, ".grm.N.bin", sep="")
    id.file.name <- paste(rootname, ".grm.id", sep="")
    write.table(grm$id, id.file.name, row=F, col=F, qu=F)
    n <- dim(grm$id)[1]
    bin.file <- file(bin.file.name, "wb")
    writeBin(grm$grm$grm, bin.file, size=4)
    close(bin.file)
    n.file <- file(n.file.name, "wb")
    writeBin(grm$grm$N, n.file, size=4)
    close(n.file)
}


readGRM<-function(rootname)
{
    bin.file.name <- paste(rootname, ".grm.bin", sep="")
    n.file.name <- paste(rootname, ".grm.N.bin", sep="")
    id.file.name <- paste(rootname, ".grm.id", sep="")
    
    cat("Reading IDs\n")
    id <- read.table(id.file.name)
    n <- dim(id)[1]
    cat("Reading GRM\n")
    bin.file <- file(bin.file.name, "rb")
    grm <- readBin(bin.file, n=n*(n+1)/2, what=numeric(0), size=4)
    close(bin.file)
    cat("Reading N\n")
    n.file <- file(n.file.name, "rb")
    N <- readBin(n.file, n=n*(n+1)/2, what=numeric(0), size=4)
    close(n.file)
    
    cat("Creating data frame\n")
    l <- list()
    for(i in 1:n)
    {
        l[[i]] <- 1:i
    }
    col1 <- rep(1:n, 1:n)
    col2 <- unlist(l)
    grm <- data.frame(id1=col1, id2=col2, N=N, grm=grm)	
    
    ret <- list()
    ret$grm <- grm
    ret$id <- id
    return(ret)
}

