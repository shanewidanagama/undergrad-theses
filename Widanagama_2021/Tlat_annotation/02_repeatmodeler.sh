#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=rptmdlr
#SBATCH --cpus-per-task=16
#SBATCH --mem=32G
#SBATCH --time=0-48:00
#SBATCH --mail-user=shanewidanagama@trentu.ca
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
# time (DD-HH:MM)

#load required modules
module load gcc/9.3.0 rmblast/2.10.0
module load repeatmodeler/2.0.1

#run BuildDatabase locally from commmand line
BuildDatabase -name t_latifolia.DB -engine rmblast ../../Pilon/t1/t_latifolia_polished.fasta

#run repeatmodeler using the database, the ncbi search engine, and 16 threads
RepeatModeler -database t_latifolia.DB -engine ncbi -pa 16
RepeatClassifier -consensi RM_7274.SatMay221119482021-a/consensi.fa -engine ncbi
