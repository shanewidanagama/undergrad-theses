#perform quality control on Illumina reads
module load fastqc
for f in `ls *.fastq.gz`
do
fastqc $f
done
