#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=pltns_contigm1
#SBATCH --cpus-per-task=32
#SBATCH --mem=400G
#SBATCH --time=0-96:00
#SBATCH --mail-user=shanewidanagama@trentu.ca
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
# time (DD-HH:MM)

#load required modules
module load gcc/7.3.0
module load platanus/1.2.4

#assemble the paired end short reads using 32 threads, and a 368 GB memory limit for making the kmer distribution
platanus assemble \
   -o platanus \
   -f SRR7881463_1P.fastq SRR7881463_2P.fastq SRR7881465_1P.fastq SRR7881465_2P.fastq SRR7881466_1P.fastq SRR7881466_2P.fastq \
   -t 32 \
   -m 368 \
   -tmp /home/shane88/scratch
