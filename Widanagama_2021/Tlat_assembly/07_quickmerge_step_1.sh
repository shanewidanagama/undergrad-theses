#!/bin/bash
#SBATCH --mem=16G
#SBATCH --cpus-per-task=16
#SBATCH --time=0-01:00      # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --mail-type=ALL
#SBATCH --mail-user=shanewidanagama@trentu.ca



#first remove whitespace from Canu genome assembly
#sed 's/ /_/g' ../../Canu_Assemblies/t2/tl-pacbio/tl.contigs.fasta > canu_no_white.fasta

#exports path variable to the output and NUCmer directory
export PATH=/home/shane88/projects/rrg-shaferab/shane88/tlatifolia/Quickmerge_Assemblies/t7_d2c+canu_reproduced:/home/shane88/projects/rrg-shaferab/shane88/tlatifolia/Quickmerge_Assemblies/t7_d2c+canu_reproduced/MUMmer3.23:$PATH

#aligns canu assembly to DBG2OLC assembly
nucmer -l 100 -prefix out  canu_no_white.fasta ../../../AROGYAPACHA/DBG2OLC_Assembler/pltns_cns/backbone_raw.fasta
delta-filter -r -q -l 10000 out.delta > out.rq.delta
#runs quickmerge with DBG2OLC assembly 1 (100% PE Illumina reads) as the query assembly and Canu assembly as the reference assembly
# with 5.0 anchor contig overlap cutoff, 1.5 overlap cutoff of contigs extending anchor contig, 8706000 bp length cutoff of anchor cutoff (based on N50 of reference assembly), and  20000 bp minimum merging length 
quickmerge -d out.rq.delta -q ../../../AROGYAPACHA/DBG2OLC_Assembler/pltns_cns/backbone_raw.fasta -r canu_no_white.fasta -hco 5.0 -c 1.5 -l 8706000 -ml 20000 -p canu+dbg2olc
