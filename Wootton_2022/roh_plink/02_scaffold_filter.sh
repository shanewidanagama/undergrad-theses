#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=scaffold_filter
#SBATCH --mem 24G
#SBATCH --time=0-01:00
#SBATCH --output=%x-%j.out
#SBATCH --mail-user=ericwootton@trentu.ca
#SBATCH --mail-type=ALL

#Create a list of scaffolds greater than 10MB
sort mountain_goat.fasta.fai -k 2 -V | awk '{if($2>10000000)print$1}' > seqs_to_keep.txt

#Separate plinkinput.tped into a separate file for each scaffold greater than 10MB
input="seqs_to_keep.txt"
while IFS= read -r line
do
  echo "$line" | awk 'match($0,/\y'$line'\y/)' plinkinput.tped > $line.tped
done < "$input"

#Separate plinkinput.tfam into a separate file for each scaffold greater than 10MB
input="seqs_to_keep.txt"
while IFS= read -r line
do
  cp plinkinput.tfam $line.tfam
done < "$input"
