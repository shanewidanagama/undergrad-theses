#!/bin/bash

#These are all basic commands that can be typed directly into the command line

#Display the number of runs of homozygosity per scaffold
for f in *.hom.indiv
do
awk '{ total += $4; count++ } END { print total/count }' ${f}
done

#Display the AVERAGE number of runs per scaffolds
awk '{ total += $4; count++ } END { print total/count }' *.hom.indiv

#Find the toal length of all the scaffolds greater than 10MB
sort mountain_goat.fasta.fai -k 2 -V | awk '{if($2>10000000)print$1,$2}' > scaffold_lengths.txt
awk '{s+=$2}END{print s}' scaffold_lengths.txt

#Load R
module load r
R

#Create a table with %ROH per individual (35 individuals, total length of scaffolds used: 2433697063 bp)
all_roh <- read.table("roh_sorted_indiv.txt", quote="\"", comment.char="")
df_roh<-NULL
for(i in 1:35) {
temp=subset(all_roh, V1==i)
ind_roh<-cbind(i,(sum(temp$V2)*1000)/2433697063)
df_roh<-rbind(df_roh,ind_roh)
}
write.table(df_roh, file = "df_roh")

#Display the table
df_roh

#Plot this table with your software of choice
